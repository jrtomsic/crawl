#!/bin/bash

cd crawl-ref/source/
make mac-app-tiles TILES=y
mv build/app-bundle-stage/*.app ../../
mv mac-app-zips/*.zip ../../
cd -
